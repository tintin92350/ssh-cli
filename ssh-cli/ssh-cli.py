import argparse
import sys
from pathlib import Path


class Host:
    host_name: str
    user: str
    preferred_authentications: str
    identity_file: str
    port: int

    metadata_name: str

    def __init__(self, name: str):
        self.host_name = ''
        self.user = ''
        self.preferred_authentications = ''
        self.identity_file = ''
        self.port = -1

        self.metadata_name = name

    def __str__(self):
        return self.host_name


def under_score(string):
    new_string = ""
    for i in range(len(string)):
        if string[i].isupper() and i > 0:
            new_string += "_" + string[i]
        else:
            new_string += string[i]
    return new_string


class SshConfig:
    hosts: dict[str, Host]

    def __init__(self):
        self.hosts = {}

    def __str__(self):
        return self.hosts.__str__()

    def add_host(self, add_host_name) -> None:
        self.hosts[add_host_name] = Host(add_host_name)

    def set_host_property(self, query_host_name, property_key: str, property_value: any):
        if query_host_name in self.hosts:
            formatted_property_key: str = under_score(property_key).lower()
            found_host: Host = self.hosts[query_host_name]

            setattr(found_host, formatted_property_key, property_value)


def read_ssh_config_from_file(filepath: str) -> SshConfig:
    ssh_config_creating: SshConfig = SshConfig()
    current_host_name: str = ''

    with open(filepath, 'r') as ssh_config_file:
        for line in ssh_config_file:

            line_formatted: str = line.lstrip(" ")

            if not line_formatted.startswith("#"):
                if line_formatted.startswith("Host "):
                    query_host_name = line_formatted.removeprefix("Host ")
                    query_host_name = query_host_name.removesuffix("\n")
                    current_host_name = query_host_name
                    ssh_config_creating.add_host(query_host_name)
                elif not line_formatted.isspace():
                    query_host_properties: [] = line_formatted.split(" ")

                    if len(query_host_properties) >= 2:
                        ssh_config_creating.set_host_property(current_host_name, query_host_properties[0],
                                                              query_host_properties[1].rstrip("\n"))

    return ssh_config_creating


if len(sys.argv) < 2:
    print('You need to specify a command')
    sys.exit()

command = sys.argv[1]

ssh_config_file_path = str(Path.home()) + "\\.ssh\\config"
ssh_config: SshConfig = read_ssh_config_from_file(ssh_config_file_path)

list_parser = argparse.ArgumentParser(description='List all host of ssh config file')

list_parser.add_argument('list', help='Show all details')
list_parser.add_argument('-d', '--details', action='store_true', help='Show all details')

args = list_parser.parse_args()

if args.list == 'list':
    print("list of hosts:")
    print("----------------------------")

    for host_name in ssh_config.hosts:
        host: Host = ssh_config.hosts[host_name]

        print('Host: ', host)

        if args.details:
            host_properties = [a for a in dir(host) if not a.startswith('__')]

            for host_property in host_properties:
                print("\t", host_property, ":", getattr(host, host_property))
